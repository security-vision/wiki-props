# Props list

Simple page to interact with the `sv-app` API and build a list of properties from each category of the wiki.
See: [https://www.securityvision.io/viz/props](https://www.securityvision.io/viz/props)
