const categories = [
        "City",
        "Country",
        "Dataset",
        "Deployments",
        "Document",
        "Events",
        "Institution",
        "Person",
        "Products",
        "Technology_Type",
        ]

Promise.all(
    categories.map(async (category) => 
            await fetch(`https://www.securityvision.io/sv-app/api/v1/properties/${category}`)
        .then(r=>r.json()).then(r=> display(category, r))
    ))

const display = (category, properties) => {
    const c = document.querySelector('.reference .category').cloneNode(true)
    c.querySelector('.title').innerHTML = category
    properties.forEach(p => {
        e = document.createElement('li')
        e.innerHTML = p
        c.querySelector('.properties')?.appendChild(e)
    })
    document.querySelector('#container')?.appendChild(c)
} 

